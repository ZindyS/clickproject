package com.example.somehomework

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val databaseReference =
            FirebaseDatabase.getInstance().reference.child("clicks")
        val messageListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val long = dataSnapshot.getValue(Long::class.java)
                    textView.text = long!!.toString()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
            }
        }

        databaseReference!!.addValueEventListener(messageListener)
    }

    fun onButtonClick(v: View) {
        val db = FirebaseDatabase.getInstance()
        val ref = db.reference.child("clicks")
        val text = textView.text
        ref.setValue(text.toString().toLong() + 1)
    }
}